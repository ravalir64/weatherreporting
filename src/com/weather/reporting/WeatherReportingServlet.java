package com.weather.reporting;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;

/**
 * Servlet implementation class WeatherReportingServlet
 */
@WebServlet("/WeatherReportingServletPath")
public class WeatherReportingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
				String zipCode = request.getParameter("zipCode");
				
		
				
				System.out.print(zipCode);
				
			    String latLongs[];
				try {
					latLongs = getLatLongPositions(zipCode);
					System.out.println("Latitude: "+latLongs[0]+" and Longitude: "+latLongs[1]);
					String responce = getWeather(latLongs);
					
					
					 JSONParser parser = new JSONParser();

					 try {

				            Object obj = parser.parse(responce);

				            JSONObject jsonObject = (JSONObject) obj;
				            System.out.println(jsonObject);

				            String latitude = (String) jsonObject.get("latitude").toString();
				            System.out.println("Latitude : " + latitude);

				            String longitude = (String) jsonObject.get("longitude").toString();
				            System.out.println("Longitude : " + longitude);
				            
				            String timeZone = (String) jsonObject.get("timezone").toString();
				            System.out.println("Time Zone : " + timeZone);

				            Object current = jsonObject.get("currently");
				            System.out.println("Currently ");
				            
				            JSONObject currentObject = (JSONObject) current;
				            String summary = (String) currentObject.get("summary").toString();
				            System.out.println("Summary : " + summary);
				            
				            String temperature = (String) currentObject.get("temperature").toString();
				            System.out.println("temperature : " + temperature);
				        
				            
				            String visibility = (String) currentObject.getOrDefault("visibility", "").toString();
				            System.out.println("visibility : " + visibility);
				            
				            String windSpeed = (String) currentObject.get("windSpeed").toString();
				            System.out.println("windSpeed : " + windSpeed);
				            
				            String humidity = (String) currentObject.get("humidity").toString();
				            System.out.println("humidity : " + humidity);
				            
				            String pressure = (String) currentObject.get("pressure").toString();
				            System.out.println("pressure : " + pressure);
				            
				            
				            
				            
				            
				            response.setContentType("text/html");
				            PrintWriter out = response.getWriter();

				            HtmlUtils hu = new HtmlUtils();

				            out.print(hu.createHtmlHeader("Weather Report"));

				            out.print(hu.getTableHead("center", 1,"Currently"));

				            out.print(hu.getTH("center", "Latitude"));
				            out.print(hu.getTH("center", "Longitude"));
				            out.print(hu.getTH("center", "Time Zone"));
				            out.print(hu.getTH("center", "Summary"));
				            out.print(hu.getTH("center", "Temperature"));
				            out.print(hu.getTH("center", "Visibility"));
				            out.print(hu.getTH("center", "Wind Speed"));
				            out.print(hu.getTH("center", "Humidity"));
				            out.print(hu.getTH("center", "Pressure"));

				            Vector av = new Vector();

				            av.addElement(latitude);
				            av.addElement(longitude);
				            av.addElement(timeZone);
				            av.addElement(summary);
				            av.addElement(temperature);
				            av.addElement(visibility);
				            av.addElement(windSpeed);
				            av.addElement(humidity);
				            av.addElement(pressure);

				           

				            out.print(hu.getTableContents("center", av, 9));
				            
				            
				            
				            
				            Object hourly = jsonObject.get("hourly");
				            JSONObject hourlyObject = (JSONObject) hourly;
				            String hourlySummary = (String) hourlyObject.get("summary");
				            System.out.println("hourlySummary : " + hourlySummary);
				           
				            JSONArray  hourlyDataArray = (JSONArray ) hourlyObject.get("data");
				            System.out.println("DataString : " + hourlyDataArray.toString());
				           
				            out.print(hu.getTableHead("center", 1,"Hourly"));
				            
				            out.print(hu.getTH("center", "Sr.No"));
				            out.print(hu.getTH("center", "Time"));
				            out.print(hu.getTH("center", "Summary"));
				            out.print(hu.getTH("center", "Temperature"));
				            out.print(hu.getTH("center", "Visibility"));
				            out.print(hu.getTH("center", "Wind Speed"));
				            out.print(hu.getTH("center", "Humidity"));
				            out.print(hu.getTH("center", "Pressure"));
				            		
				            	
				            Vector avHourly = new Vector();
				            if(hourlyDataArray.size() == 0)
				            {
				            	avHourly.addElement("No data available");
				            }
				            else{
				            	System.out.println(hourlyDataArray.size());
				            	System.out.println(hourlyDataArray.toString());
				            for (int i = 0 ; i < hourlyDataArray.size(); i++) {
				                JSONObject dataObj = (JSONObject) hourlyDataArray.get(i);
				                String dataSummary = (String) dataObj.get("summary");
				                String datavisibility = (String) dataObj.getOrDefault("visibility", "").toString();
				                
				                String datapressure = (String) dataObj.getOrDefault("pressure","").toString();
				                String datatemperature = (String) dataObj.getOrDefault("temperature","").toString();
				                String datahumidity = (String) dataObj.getOrDefault("humidity","").toString();
				                String datatime = (String) dataObj.getOrDefault("time","").toString();
				                String datawindSpeed = (String) dataObj.getOrDefault("windSpeed","").toString();
				                
				                long time = Long.parseLong( datatime );
				                Date expiry = new Date( time * 1000 );
				                
				                avHourly.addElement(i+1);
				                avHourly.addElement(expiry);
				                avHourly.addElement(dataSummary);
				                avHourly.addElement(datatemperature);
				                avHourly.addElement(datavisibility);
				                avHourly.addElement(datawindSpeed);
				                avHourly.addElement(datahumidity);
				                avHourly.addElement(datapressure);

					          
				            }
				            }
				            out.print(hu.getTableContents("center", avHourly, 8));
				            out.print(hu.getHtmlFooter());
					 }

				         catch (ParseException e) {
				        	 System.out.println("Done1");
				        }

					
					
					
					System.out.println("Done");
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.print(e);
					e.printStackTrace();
				}
			   // System.out.println("Latitude: "+latLongs[0]+" and Longitude: "+latLongs[1]);
				
				
				
	}
	
	public String getWeather(String[] latLong) throws Exception
	  {
	    int responseCode = 0;
	    
	    String api = "https://api.darksky.net/forecast/6eae2b2942a71d7d2f36f4f1ccb9db2e/" +latLong[0] +","+ latLong[1] ;
	    URL url = new URL(api);
	    HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
	    httpConnection.connect();
	    responseCode = httpConnection.getResponseCode();
	    
	    if(responseCode == 200)
	    {
	    	System.out.println("Success");
	    	InputStream is = null;
	        try {
	          is = httpConnection.getInputStream();
	          int ch;
	          StringBuffer sb = new StringBuffer();
	          while ((ch = is.read()) != -1) {
	            sb.append((char) ch);
	          }
	          System.out.println(sb.toString());
	          return sb.toString();
	        } catch (IOException e) {
	          throw e;
	        }
	    	
	    }
	      else
	      {
	         throw new Exception("Error from the API - response status: ");
	      }
	    
	  }
	
	
	
	  public static String[] getLatLongPositions(String address) throws Exception
	  {
	    int responseCode = 0;
	    String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + URLEncoder.encode(address, "UTF-8") + "&sensor=true";
	    URL url = new URL(api);
	    HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
	    httpConnection.connect();
	    responseCode = httpConnection.getResponseCode();
	    if(responseCode == 200)
	    {
	      DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();;
	      Document document = builder.parse(httpConnection.getInputStream());
	      XPathFactory xPathfactory = XPathFactory.newInstance();
	      XPath xpath = xPathfactory.newXPath();
	      XPathExpression expr = xpath.compile("/GeocodeResponse/status");
	      String status = (String)expr.evaluate(document, XPathConstants.STRING);
	      if(status.equals("OK"))
	      {
	         expr = xpath.compile("//geometry/location/lat");
	         String latitude = (String)expr.evaluate(document, XPathConstants.STRING);
	         expr = xpath.compile("//geometry/location/lng");
	         String longitude = (String)expr.evaluate(document, XPathConstants.STRING);
	         return new String[] {latitude, longitude};
	      }
	      else
	      {
	         throw new Exception("Error from the API - response status: "+status);
	      }
	    }
	    return null;
	  }

}
